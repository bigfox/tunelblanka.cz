var gulp = require('gulp'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css'),
    base64 = require('gulp-base64'),
    template = require('gulp-template');

var files = {
    js: ['bower_components/jquery/dist/jquery.js', 'bower_components/fancybox/source/jquery.fancybox.js', 'bower_components/fancybox/source/helpers/jquery.fancybox-thumbs.js', 'src/index.js'],
    css: ['bower_components/fancybox/source/jquery.fancybox.css', 'bower_components/fancybox/source/helpers/jquery.fancybox-thumbs.css'],
    img: ['src/*.png'],
    manifest: ['src/manifest.json']
};

// Cleanup
gulp.task('clean', function() {
    return gulp.src('dist/*', {
        read: false
    }).pipe(clean());
});

// Minify css
gulp.task('css', ['clean'], function() {
    return gulp.src(files.css)
        .pipe(base64())
        .pipe(minify({
            keepSpecialComments: 0
        }))
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('dist'))
});

// Uglify js
gulp.task('js', ['clean'], function() {
    return gulp.src(files.js)
        .pipe(uglify())
        .pipe(concat('index.js'))
        .pipe(gulp.dest('dist'));
});

// Images
gulp.task('img', ['clean'], function() {
    return gulp.src(files.img).pipe(gulp.dest('dist'));
});

// Manifest
gulp.task('manifest', ['clean'], function() {
    return gulp.src(files.manifest)
        .pipe(template({package: require('./package.json')}))
        .pipe(gulp.dest('dist'));
});

// Build
gulp.task('build', ['clean', 'css', 'js', 'img', 'manifest']);

gulp.task('watch', ['build'], function() {
    gulp.watch(files.css, ['build']);
    gulp.watch(files.js, ['build']);
});

gulp.task('default', ['build']);