# tunelblanka.cz

Opera extension for improving photo gallery on [tunelblanka.cz](http://www.tunelblanka.cz.cz) site.

Extension only for Opera 15 or higher.

## Development
Using `npm`, `bower`, `gulp`

1. Install dependencies

        npm install
        bower install

2. Build

        gulp build
