$(function() {
	var boxes = $('table.innerTab4Text').find('td.thumbnail');

	boxes.children('a').each(function() {
		var link = $(this);
		var id = link.attr('href').match(/\'(\d*)\'/)[1];

		link.attr({
			id: id,
			rel: 'gallery',
			href: 'javascript:void(0)'
		}).addClass('gallery');
	});

	$('a.gallery').fancybox({
		loop: false,
		openEffect: 'none',
		closeEffect: 'none',
		nextEffect: 'none',
		prevEffect: 'none',
		live: false,
		helpers: {
			overlay: {
				locked: true
			},
			title: {
				type: 'inside'
			},
			thumbs: {
				width: 50,
				height: 50,
				position: 'bottom'
			}
		},
		beforeLoad: function() {
			var self = this;

			$.ajax({
				url: 'pic_fotogalerie.php?photo=' + $(self.element).attr('id'),
				async: false,
				success: function(data, textStatus, jqXHR) {
					self.href = data.match(/<img class=\"imgBorder\" src=\"(.*)\" onClick=\".*\">/)[1];
					self.type = 'image';
				}
			});

			return true;
		}
	});
})